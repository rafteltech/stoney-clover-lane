class ProductForm extends HTMLElement {
  constructor() {
    super();

    this.form = this.querySelector("form");
    this.form.addEventListener("submit", this.onSubmitHandler.bind(this));
    this.cartNotification = document.querySelector("cart-notification");
  }

  onSubmitHandler(evt) {
    evt.preventDefault();

    this.cartNotification.setActiveElement(document.activeElement);

    const productAddedEvent = new Event("product:added");
    const quantityContainer = this.form.closest('.product__form').parentNode;
    const submitButton = this.querySelector('[type="submit"]');
    const buttonText = submitButton.querySelector('span');
    const icon = document.querySelector('.header__icon--cart');
    let quantity = document.querySelector('.quantity__input').value;

    if (quantityContainer.querySelector(".quantity__input")) {
      quantity = quantityContainer.querySelector(".quantity__input").value || quantity;
    }

    // Find current swatch to get the SKU
    let currentSwatch = document.querySelector('.swatch input:checked');
    if (!currentSwatch) {
      const id = document.querySelector('[data-type="add-to-cart-form"] input[name="id"]').value;
      currentSwatch = document.querySelector(`.product-select option[value="${id}"]`);
    }


    const canUpdateQuantity = async (skusToValidate) => {
      let _canUpdateQuantity = true;
      if (window.QuantityLimit) {
        try {
          _canUpdateQuantity = await window.QuantityLimit.validateQuantityLimit(skusToValidate);
        } catch (e) {
          console.log(e);
        }
      }
      return _canUpdateQuantity;
    }

    const formData = JSON.parse(serializeForm(this.form));

    // Add embroidered property to form data. Pending to create a more performant solution to support custom properties.
    let properties = {};
    const embroideredName = document.querySelector('.embroidered__input');
    const preorder = document.querySelector('input[name="properties[Pre-order]"]');
    if (embroideredName) {
      properties['Name'] = embroideredName.value;
    }
    if (preorder) {
      properties['Pre-order'] = preorder.value;
    }

    // Add gift card properties
    const gcContainer = document.querySelector('[data-gift-card-container]');
    if (gcContainer) {
      const inputErrorMesasge = gcContainer.querySelector('.inputs-required-error');
      const gcRecipient = gcContainer.querySelector('#gc_recipient').value;
      const gcMessage = gcContainer.querySelector('#gc_message').value;
        
      inputErrorMesasge.classList.remove('show');
      
      // validate empty fields
      if (!gcRecipient || !gcMessage) {
        inputErrorMesasge.classList.add('show');
        return false;
      }
      
      // validate gift card fields before adding to cart return false if not valid
      if (gcContainer.querySelector('#gc_recipient').classList.contains('error')) {
        gcContainer.querySelector('#gc_recipient').classList.add('red-border');
        gcContainer.querySelector('#gc_recipient').focus();
        return false;
      }      
      if (gcRecipient !== '') properties['GC Recipient'] = gcRecipient;
      if (gcMessage !== '') properties['GC Message'] = gcMessage;
    }
    
    canUpdateQuantity({
      [currentSwatch.dataset.sku]: quantity
    }).then(canUpdate => {

      if (canUpdate) {

        submitButton.setAttribute("disabled", true);
        submitButton.classList.add("loading");
        buttonText.dataset.text = buttonText.innerText;
        buttonText.innerText = "Adding";

        const body = JSON.stringify({
          ...formData,
          quantity,
          properties,
          sections: this.cartNotification.getSectionsToRender().map((section) => section.id),
          sections_url: window.location.pathname,
        });


        fetch(`${routes.cart_add_url}`, {
            ...fetchConfig("javascript"),
            body
          })
          .then((response) => response.json())
          .catch((e) => {
            console.error(e);
          })
          .finally((response) => {
            submitButton.classList.remove("loading");
            submitButton.removeAttribute("disabled");
            buttonText.innerText = "Added";
            document.dispatchEvent(productAddedEvent);
            //console.log(response)



            if (icon) {
              icon.classList.add("active")
              //update number in header
              fetch('/cart.js')
                .then(response => response.json())
                .then(data => {
                  icon.querySelectorAll('.cart-count-bubble span').forEach(el => {
                    el.innerText = data.item_count
                  })
                  console.log(data)
                });
            }
            setTimeout(() => {
              buttonText.innerText = buttonText.dataset.text;
              if (icon) {
                icon.classList.remove("active")
              }
            }, 1000)
          });
      }
    });
  }
}
customElements.define("product-form", ProductForm);