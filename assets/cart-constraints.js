(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;
module.exports["default"] = module.exports, module.exports.__esModule = true;
},{}],2:[function(require,module,exports){
/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function define(obj, key, value) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
    return obj[key];
  }
  try {
    // IE 8 has a broken Object.defineProperty that only works on DOM objects.
    define({}, "");
  } catch (err) {
    define = function(obj, key, value) {
      return obj[key] = value;
    };
  }

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  define(IteratorPrototype, iteratorSymbol, function () {
    return this;
  });

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = GeneratorFunctionPrototype;
  define(Gp, "constructor", GeneratorFunctionPrototype);
  define(GeneratorFunctionPrototype, "constructor", GeneratorFunction);
  GeneratorFunction.displayName = define(
    GeneratorFunctionPrototype,
    toStringTagSymbol,
    "GeneratorFunction"
  );

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      define(prototype, method, function(arg) {
        return this._invoke(method, arg);
      });
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      define(genFun, toStringTagSymbol, "GeneratorFunction");
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  define(AsyncIterator.prototype, asyncIteratorSymbol, function () {
    return this;
  });
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  define(Gp, toStringTagSymbol, "Generator");

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  define(Gp, iteratorSymbol, function() {
    return this;
  });

  define(Gp, "toString", function() {
    return "[object Generator]";
  });

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
  typeof module === "object" ? module.exports : {}
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, in modern engines
  // we can explicitly access globalThis. In older engines we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  if (typeof globalThis === "object") {
    globalThis.regeneratorRuntime = runtime;
  } else {
    Function("r", "regeneratorRuntime = r")(runtime);
  }
}

},{}],3:[function(require,module,exports){
module.exports = require("regenerator-runtime");

},{"regenerator-runtime":2}],4:[function(require,module,exports){
"use strict";

var _qtyLimit = _interopRequireDefault(require("./partials/qty-limit"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

window.addEventListener("load", function () {
  _qtyLimit["default"].init();
});

},{"./partials/qty-limit":5}],5:[function(require,module,exports){
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _cart = require("../util/cart");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

var QuantityLimit = /*#__PURE__*/function () {
  function QuantityLimit() {
    _classCallCheck(this, QuantityLimit);

    window.SclProducts = window.SclProducts || {};
  }

  _createClass(QuantityLimit, [{
    key: "init",
    value: function init() {
      console.log("QtyLimit Initialised");

      if (document.querySelector('[data-section-type="cart-items"]') !== null && window.SclProducts.invalidCartItems) {
        this.disableCheckoutEvents();
      }
    }
  }, {
    key: "disableCheckoutEvents",
    value: function disableCheckoutEvents() {
      var _this = this;

      var checkoutBtn = document.querySelector("#checkout"),
          checkoutButtons = document.querySelector(".additional-checkout-buttons");

      if (checkoutBtn) {
        checkoutBtn.addEventListener("click", function (evt) {
          evt.preventDefault();

          _this.showOverlay();
        });
      }

      if (checkoutButtons) {
        checkoutButtons.remove();
      }
    }
  }, {
    key: "validateQuantityLimit",
    value: function () {
      var _validateQuantityLimit = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee(skusToValidate) {
        var showErrorOverlay,
            _self,
            cart,
            sitewideQtyLimit,
            skusToAdd,
            isCartPage,
            cartSkus,
            foundLimitedSkus,
            _args = arguments;

        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                showErrorOverlay = _args.length > 1 && _args[1] !== undefined ? _args[1] : true;
                _self = this;
                _context.next = 4;
                return (0, _cart.getItems)();

              case 4:
                cart = _context.sent;
                sitewideQtyLimit = window.SclProducts.sitewideQtyLimit || -1;
                skusToAdd = Object.keys(skusToValidate);
                isCartPage = document.querySelector('[data-section-type="cart-items"]') !== null;
                console.log(isCartPage);
                cartSkus = cart.items.reduce(function (skus, item) {
                  var customizedPatch = false;
                  var isPatch = item.title.indexOf("Patch") > -1;

                  if (item !== null && item !== void 0 && item.properties) {
                    var properties = Object.keys(item.properties);
                    customizedPatch = isPatch && "_Timestamp" in properties;
                  }

                  if (!customizedPatch) {
                    if (!(item.sku in skus)) {
                      skus[item.sku] = item.quantity;
                    } else {
                      skus[item.sku] += item.quantity;
                    }
                  }

                  return skus;
                }, []);
                foundLimitedSkus = skusToAdd.reduce(function (foundSkus, sku) {
                  // Process SKU if and only if the qty is limited
                  if (sitewideQtyLimit > 0) {
                    var qtyToValidate = isCartPage ? parseInt(skusToValidate[sku]) : parseInt(skusToValidate[sku]) + (cartSkus[sku] || 0);

                    if (qtyToValidate > sitewideQtyLimit) {
                      foundSkus.push(sku);
                    }
                  }

                  return foundSkus;
                }, []);

                if (foundLimitedSkus.length && showErrorOverlay && sitewideQtyLimit > 0) {
                  _self.showOverlay();
                }

                return _context.abrupt("return", !foundLimitedSkus.length);

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function validateQuantityLimit(_x) {
        return _validateQuantityLimit.apply(this, arguments);
      }

      return validateQuantityLimit;
    }()
  }, {
    key: "showOverlay",
    value: function showOverlay() {
      var overlayType = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "sitewide";
      var overlayData = window.SclProducts.notice || {};
      overlayData.notice = overlayData[overlayType];
      overlayData.overlayType = overlayType;
      overlayData.id = "".concat(overlayType, "QuantityLimitModal");
      overlayData["class"] = "".concat(overlayType, "-quantity-limit-modal");

      var overlayEl = this._createOverlayElement(overlayData);

      if (overlayEl) {
        overlayEl.classList.add("active");
      }
    }
  }, {
    key: "_cloneElement",
    value: function _cloneElement(elem) {
      var newElem = elem.cloneNode(true);
      newElem.originalEl = elem;
      newElem.dataset.validateAddToCart = true;
      elem.style.display = "none";
      elem.classList.add("visuallyhidden");
      elem.parentElement.appendChild(newElem);
      return newElem;
    }
  }, {
    key: "_createOverlayElement",
    value: function _createOverlayElement(overlayData) {
      var overlayEl = document.getElementById(overlayData.id);

      if (!overlayEl) {
        var overlayDiv = document.createElement("div"),
            overlayHtml = this._getOverlayTemplate(overlayData);

        overlayDiv.innerHTML = overlayHtml;
        overlayEl = overlayDiv.firstChild;

        var closeOverlayHandler = function closeOverlayHandler(evt) {
          evt.preventDefault();
          overlayEl.classList.remove("active"); // Reload the page to reset values and settings

          location.reload();
        };

        overlayDiv.querySelector("button.cancel").addEventListener("click", closeOverlayHandler);
        overlayDiv.querySelector("button.close").addEventListener("click", closeOverlayHandler);
        document.body.appendChild(overlayEl);
      }

      return overlayEl;
    }
  }, {
    key: "_getOverlayTemplate",
    value: function _getOverlayTemplate(overlayData) {
      return "<div class=\"".concat(overlayData["class"], "\" id=\"").concat(overlayData.id, "\">\n                    <button type=\"button\" class=\"close\">\n                        <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"h-6 w-6\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">\n                            <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M6 18L18 6M6 6l12 12\"></path>\n                        </svg>\n                    </button>\n                    <h3>").concat(overlayData.notice.headline, "</h3>\n                    <p>").concat(overlayData.notice.message, "</p>\n                    <div class=\"actions\">\n                        <button type=\"button\" class=\"cancel\">").concat(overlayData.notice.button_text, "</button>\n                    </div>\n                </div>");
    }
  }]);

  return QuantityLimit;
}();

var QtyLimit = new QuantityLimit(); // Expose quantity limit validation function to global scope

window.QuantityLimit = QtyLimit;
var _default = QtyLimit;
exports["default"] = _default;

},{"../util/cart":6,"@babel/runtime/helpers/interopRequireDefault":1,"@babel/runtime/regenerator":3}],6:[function(require,module,exports){
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.saveForLater = exports.validateGiftWrapProductCount = exports.updateGiftWrappingProduct = exports.checkGiftWrapVisibility = exports.getItems = exports.removeItems = exports.addItems = exports.updateItemQuantity = exports.updateItemProperties = exports.updateCartAttributes = exports.disableLoadingOverlay = exports.enableLoadingOverlay = exports.GIFT_WRAP_VARIANT_ID = void 0;

var _saveAPI = require("./saveAPI");

function _createForOfIteratorHelper(o, allowArrayLike) {
  var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"];

  if (!it) {
    if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
      if (it) o = it;
      var i = 0;

      var F = function F() {};

      return {
        s: F,
        n: function n() {
          if (i >= o.length) return {
            done: true
          };
          return {
            done: false,
            value: o[i++]
          };
        },
        e: function e(_e) {
          throw _e;
        },
        f: F
      };
    }

    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  var normalCompletion = true,
      didErr = false,
      err;
  return {
    s: function s() {
      it = it.call(o);
    },
    n: function n() {
      var step = it.next();
      normalCompletion = step.done;
      return step;
    },
    e: function e(_e2) {
      didErr = true;
      err = _e2;
    },
    f: function f() {
      try {
        if (!normalCompletion && it["return"] != null) it["return"]();
      } finally {
        if (didErr) throw err;
      }
    }
  };
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _iterableToArray(iter) {
  if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return _arrayLikeToArray(arr);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) {
    arr2[i] = arr[i];
  }

  return arr2;
}

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

var IS_SCL_DEV = window.location.hostname === "stoney-clover-dev.myshopify.com";
var IS_TOMORROW_DEV = window.location.hostname === "stoneycloverlane-development.myshopify.com" || window.location.hostname === "127.0.0.1";
var GIFT_WRAP_VARIANT_ID = IS_SCL_DEV ? "31069094150249" : IS_TOMORROW_DEV ? "40425155264701" //tomorrow variant ID
: "4347286061086"; //production variant ID

exports.GIFT_WRAP_VARIANT_ID = GIFT_WRAP_VARIANT_ID;

var enableLoadingOverlay = function enableLoadingOverlay() {
  var el = document.querySelector(".cart-sections-wrapper");
  if (el) el.classList.add("loading");
};

exports.enableLoadingOverlay = enableLoadingOverlay;

var disableLoadingOverlay = function disableLoadingOverlay() {
  var el = document.querySelector(".cart-sections-wrapper");
  if (el) el.classList.remove("loading");
}; //update cart level attributes


exports.disableLoadingOverlay = disableLoadingOverlay;

var updateCartAttributes = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee(attributes) {
    var stringAttributes, body, options, response;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            enableLoadingOverlay();
            stringAttributes = JSON.stringify(attributes);
            body = "{\"attributes\":".concat(stringAttributes, "}");
            console.log(body);
            options = {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: body
            };
            _context.next = 7;
            return fetch("/cart/update.js", options);

          case 7:
            response = _context.sent;
            disableLoadingOverlay();

            if (!(response.status === 200)) {
              _context.next = 11;
              break;
            }

            return _context.abrupt("return", response);

          case 11:
            throw new Error("Failed to update attributes, Shopify returned ".concat(response.status, " ").concat(response.statusText));

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function updateCartAttributes(_x) {
    return _ref.apply(this, arguments);
  };
}(); //update line item props for one item


exports.updateCartAttributes = updateCartAttributes;

var updateItemProperties = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee2(key, quantity, properties) {
    var body, options, response;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            enableLoadingOverlay();
            body = "{\n    \"id\": \"".concat(key, "\",\n    \"quantity\": \"").concat(quantity, "\",\n    \"properties\":").concat(JSON.stringify(properties), "\n  }");
            console.log(body);
            options = {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: body
            };
            _context2.next = 6;
            return fetch("/cart/change.js", options);

          case 6:
            response = _context2.sent;
            disableLoadingOverlay();

            if (!(response.status === 200)) {
              _context2.next = 10;
              break;
            }

            return _context2.abrupt("return", response);

          case 10:
            throw new Error("Failed to update item properties, Shopify returned ".concat(response.status, " ").concat(response.statusText));

          case 11:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function updateItemProperties(_x2, _x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}(); //remove multiple items from cart then reload


exports.updateItemProperties = updateItemProperties;

var updateItemQuantity = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee3(key, quantity) {
    var options, response;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            enableLoadingOverlay();
            options = {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: "{\"updates\":{\"".concat(key, "\": ").concat(quantity, "}}")
            };
            _context3.next = 4;
            return fetch("/cart/update.js", options);

          case 4:
            response = _context3.sent;
            disableLoadingOverlay();

            if (!(response.status === 200)) {
              _context3.next = 8;
              break;
            }

            return _context3.abrupt("return", response);

          case 8:
            throw new Error("Failed to update items, Shopify returned ".concat(response.status, " ").concat(response.statusText));

          case 9:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function updateItemQuantity(_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}(); //add items to cart


exports.updateItemQuantity = updateItemQuantity;

var addItems = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee4(items) {
    var options, response;
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            options = {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify({
                items: items
              })
            };
            _context4.next = 3;
            return fetch("/cart/add.js", options);

          case 3:
            response = _context4.sent;

            if (!(response.status === 200)) {
              _context4.next = 6;
              break;
            }

            return _context4.abrupt("return", response);

          case 6:
            throw new Error("Failed to add items to cart, Shopify returned ".concat(response.status, " ").concat(response.statusText));

          case 7:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function addItems(_x7) {
    return _ref4.apply(this, arguments);
  };
}(); //remove multiple items from cart then reload


exports.addItems = addItems;

var removeItems = /*#__PURE__*/function () {
  var _ref5 = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee5(keysToRemove) {
    var gifts, updates, options, response;
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            enableLoadingOverlay();
            gifts = [];
            keysToRemove.forEach(function (k) {
              var keyEl = document.querySelector(".gift-wrap-item[data-item-key=\"".concat(k, "\"]"));
              if (keyEl) gifts.push(keyEl.dataset.key);
            });
            updates = [].concat(_toConsumableArray(keysToRemove), gifts).map(function (k) {
              return "\"".concat(k, "\": 0");
            }).join(",");
            console.log("updates", updates);
            options = {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: "{\"updates\":{".concat(updates, "}}")
            };
            _context5.next = 8;
            return fetch("/cart/update.js", options);

          case 8:
            response = _context5.sent;
            disableLoadingOverlay();

            if (!(response.status === 200)) {
              _context5.next = 12;
              break;
            }

            return _context5.abrupt("return", response);

          case 12:
            throw new Error("Failed to remove items to cart, Shopify returned ".concat(response.status, " ").concat(response.statusText));

          case 13:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function removeItems(_x8) {
    return _ref5.apply(this, arguments);
  };
}(); //remove multiple items from cart


exports.removeItems = removeItems;

var getItems = /*#__PURE__*/function () {
  var _ref6 = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee6() {
    var response;
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.next = 2;
            return fetch("/cart.js");

          case 2:
            response = _context6.sent;

            if (!(response.status === 200)) {
              _context6.next = 5;
              break;
            }

            return _context6.abrupt("return", response.json());

          case 5:
            throw new Error("Failed to get items from cart, Shopify returned ".concat(response.status, " ").concat(response.statusText));

          case 6:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));

  return function getItems() {
    return _ref6.apply(this, arguments);
  };
}(); //disable or enable main cart gift wrapping if individual items have wrapping
//if only one item in cart disable wrapping on individual items


exports.getItems = getItems;

var checkGiftWrapVisibility = function checkGiftWrapVisibility() {
  var mainCartGiftWrapContainer = document.getElementById("gift-all");
  var itemCount = document.querySelectorAll(".cart-item.product-group").length;

  if (itemCount < 2) {
    var _document$querySelect, _document$querySelect2;

    (_document$querySelect = document.querySelector("#cart-items-container")) === null || _document$querySelect === void 0 ? void 0 : (_document$querySelect2 = _document$querySelect.classList) === null || _document$querySelect2 === void 0 ? void 0 : _document$querySelect2.add("only-one-item");
  }

  var itemsWithNotesCount = document.querySelectorAll(".cart-items .gift-checkbox-wrapper.has-note").length;

  if (itemsWithNotesCount < 1) {
    mainCartGiftWrapContainer.classList.remove("disabled");
  } else {
    mainCartGiftWrapContainer.classList.add("disabled");
  }
};

exports.checkGiftWrapVisibility = checkGiftWrapVisibility;

var addGiftWrapItem = /*#__PURE__*/function () {
  var _ref7 = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee7(note, vid, key) {
    var response;
    return _regenerator["default"].wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            console.log("Adding Gift Item");
            console.log("adding with id: ".concat(vid));
            _context7.next = 4;
            return addItems([{
              quantity: 1,
              id: GIFT_WRAP_VARIANT_ID,
              properties: {
                vid: vid,
                itemKey: key,
                giftNote: note
              }
            }]);

          case 4:
            response = _context7.sent;
            return _context7.abrupt("return", response);

          case 6:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));

  return function addGiftWrapItem(_x9, _x10, _x11) {
    return _ref7.apply(this, arguments);
  };
}();

var updateGiftWrapItem = /*#__PURE__*/function () {
  var _ref8 = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee8(note, vid, key, giftProductEl) {
    var giftProductKey, response;
    return _regenerator["default"].wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            console.log("updating Gift Item");
            giftProductKey = giftProductEl.getAttribute("data-key");
            _context8.next = 4;
            return updateItemProperties(giftProductKey, 1, {
              giftNote: note,
              vid: vid,
              itemKey: key
            });

          case 4:
            response = _context8.sent;
            return _context8.abrupt("return", response);

          case 6:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8);
  }));

  return function updateGiftWrapItem(_x12, _x13, _x14, _x15) {
    return _ref8.apply(this, arguments);
  };
}();

var removeGiftWrapItem = /*#__PURE__*/function () {
  var _ref9 = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee9(giftProductEl) {
    var giftProductKey, response;
    return _regenerator["default"].wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            giftProductKey = giftProductEl.getAttribute("data-key");
            console.log("Removing Gift Item");
            console.log(giftProductKey);
            _context9.next = 5;
            return removeItems([giftProductKey]);

          case 5:
            response = _context9.sent;
            return _context9.abrupt("return", response);

          case 7:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9);
  }));

  return function removeGiftWrapItem(_x16) {
    return _ref9.apply(this, arguments);
  };
}(); //go through all the product in the cart and if they have gift wrapping enabled,
//verify a corresponding gift wrapping product exists in the cart


var updateGiftWrappingProduct = /*#__PURE__*/function () {
  var _ref10 = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee10(forceRefresh) {
    var needsRefresh, container, nonGiftProductSelector, items, disableAllItemGifts, _iterator, _step, _item$querySelector, item, itemHasNote, key, vid, note, giftProductInCart;

    return _regenerator["default"].wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            enableLoadingOverlay();
            needsRefresh = forceRefresh !== null && forceRefresh !== void 0 ? forceRefresh : false;
            container = document.querySelector(".cart-sections-wrapper");
            nonGiftProductSelector = ".cart-items .cart-item:not(.saving):not(.gift-wrap-item)";
            items = document.querySelectorAll(nonGiftProductSelector);
            disableAllItemGifts = container.classList.contains("gifts-disabled"); //add new notes

            _iterator = _createForOfIteratorHelper(items);
            _context10.prev = 7;

            _iterator.s();

          case 9:
            if ((_step = _iterator.n()).done) {
              _context10.next = 45;
              break;
            }

            item = _step.value;
            itemHasNote = item.querySelector(".gift-checkbox-wrapper.has-note");
            key = item.getAttribute("data-key");
            vid = item.getAttribute("data-vid");
            note = (_item$querySelector = item.querySelector(".note")) === null || _item$querySelector === void 0 ? void 0 : _item$querySelector.value;
            giftProductInCart = document.querySelector(".gift-wrap-item[data-item-key='".concat(key, "']"));

            if (!(disableAllItemGifts && giftProductInCart)) {
              _context10.next = 22;
              break;
            }

            needsRefresh = true;
            _context10.next = 20;
            return removeGiftWrapItem(giftProductInCart);

          case 20:
            _context10.next = 43;
            break;

          case 22:
            if (!itemHasNote) {
              _context10.next = 37;
              break;
            }

            if (!giftProductInCart) {
              _context10.next = 32;
              break;
            }

            if (!(note !== giftProductInCart.getAttribute("data-note"))) {
              _context10.next = 30;
              break;
            }

            console.log("has note gift product already in cart, updating note");
            needsRefresh = true;
            _context10.next = 29;
            return updateGiftWrapItem(note, vid, key, giftProductInCart);

          case 29:
            return _context10.abrupt("continue", 43);

          case 30:
            console.log("has note gift product already in cart, skipping");
            return _context10.abrupt("continue", 43);

          case 32:
            needsRefresh = true;
            _context10.next = 35;
            return addGiftWrapItem(note, vid, key);

          case 35:
            _context10.next = 43;
            break;

          case 37:
            if (giftProductInCart) {
              _context10.next = 40;
              break;
            }

            console.log("no note and gift product is not in cart, skipping");
            return _context10.abrupt("continue", 43);

          case 40:
            needsRefresh = true;
            _context10.next = 43;
            return removeGiftWrapItem(giftProductInCart);

          case 43:
            _context10.next = 9;
            break;

          case 45:
            _context10.next = 50;
            break;

          case 47:
            _context10.prev = 47;
            _context10.t0 = _context10["catch"](7);

            _iterator.e(_context10.t0);

          case 50:
            _context10.prev = 50;

            _iterator.f();

            return _context10.finish(50);

          case 53:
            console.log("done");
            disableLoadingOverlay();

            if (needsRefresh) {
              console.log("refreshing");
              location.reload();
            }

          case 56:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10, null, [[7, 47, 50, 53]]);
  }));

  return function updateGiftWrappingProduct(_x17) {
    return _ref10.apply(this, arguments);
  };
}(); //on load: check if the gift wrap item quantity matches the number of items with gift wrap line item property


exports.updateGiftWrappingProduct = updateGiftWrappingProduct;

var validateGiftWrapProductCount = function validateGiftWrapProductCount() {
  var _document$querySelect3, _document$querySelect4;

  var itemsWithNotesCount = (_document$querySelect3 = document.querySelectorAll(".cart-items .cart-item:not(.saving) .gift-checkbox-wrapper.has-note")) === null || _document$querySelect3 === void 0 ? void 0 : _document$querySelect3.length;
  var giftWrapItemQuantity = (_document$querySelect4 = document.querySelectorAll(".cart-item.gift-wrap-item[data-key]")) === null || _document$querySelect4 === void 0 ? void 0 : _document$querySelect4.length;
  if (isNaN(itemsWithNotesCount) || isNaN(giftWrapItemQuantity)) return;

  if (itemsWithNotesCount != giftWrapItemQuantity) {
    updateGiftWrappingProduct();
  }
};

exports.validateGiftWrapProductCount = validateGiftWrapProductCount;

var displayMessage = function displayMessage(message) {}; //save for later them remove items from cart


var saveForLater = /*#__PURE__*/function () {
  var _ref11 = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee11(data, keys) {
    return _regenerator["default"].wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            enableLoadingOverlay();
            (0, _saveAPI.saveItem)(data).then(function (response) {
              console.log("save for later");
              console.log(response);

              if (response.ok) {
                removeItems(keys).then(function (res) {
                  updateGiftWrappingProduct(true);
                });
              } else {
                displayMessage("Sorry there was a problem saving this item, please try again later");
                disableLoadingOverlay();
              }
            });

          case 2:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11);
  }));

  return function saveForLater(_x18, _x19) {
    return _ref11.apply(this, arguments);
  };
}();

exports.saveForLater = saveForLater;

},{"./saveAPI":7,"@babel/runtime/helpers/interopRequireDefault":1,"@babel/runtime/regenerator":3}],7:[function(require,module,exports){
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getItem = exports.getItemLink = exports.deleteItem = exports.saveItem = exports.getEnvironmentConfig = exports.environments = void 0;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}
/*
utilities for saving and loading pre-customized bags via the AWS endpoint
*/
//environments


var environments = {
  PROD: {
    url: "https://7q6dv7yx04.execute-api.us-east-1.amazonaws.com/dev",
    key: "kfzRoNK75R9pM9Rfcr7p3vyiEearfaR7BLZA8Pf4",
    screenshot_url: "https://6u0t12lj6l.execute-api.us-east-1.amazonaws.com/prod/screenshot",
    custom_sewing_variant_id: 4931250225182
  },
  STAGING: {
    url: "https://neqva6afs4.execute-api.us-west-2.amazonaws.com/dev",
    key: "nDonJkK2hn6DyEGec1MOC2z1jhEX4Wi45ncJJRkT",
    screenshot_url: "https://4efckk3ja2.execute-api.us-east-1.amazonaws.com/dev/screenshot",
    custom_sewing_variant_id: 31344967188589
  },
  DEVELOPMENT: {
    url: "https://7q6dv7yx04.execute-api.us-east-1.amazonaws.com/dev",
    key: "kfzRoNK75R9pM9Rfcr7p3vyiEearfaR7BLZA8Pf4",
    screenshot_url: "https://4efckk3ja2.execute-api.us-east-1.amazonaws.com/dev/screenshot",
    custom_sewing_variant_id: 31069085597801
  },
  SCL_DEV: {
    url: "https://7q6dv7yx04.execute-api.us-east-1.amazonaws.com/dev",
    key: "kfzRoNK75R9pM9Rfcr7p3vyiEearfaR7BLZA8Pf4",
    screenshot_url: "https://4efckk3ja2.execute-api.us-east-1.amazonaws.com/dev/screenshot",
    custom_sewing_variant_id: 31069085597801
  },
  TOMORROW_DEVELOPMENT: {
    url: "https://efpf7kla9c.execute-api.us-east-2.amazonaws.com/beta",
    key: "kfzRoNK75R9pM9Rfcr7p3vyiEearfaR7BLZA8Pf4",
    screenshot_url: "https://4efckk3ja2.execute-api.us-east-1.amazonaws.com/dev/screenshot",
    custom_sewing_variant_id: 40424965079229
  },
  DEFAULT: {
    url: "https://efpf7kla9c.execute-api.us-east-2.amazonaws.com/beta",
    key: "kfzRoNK75R9pM9Rfcr7p3vyiEearfaR7BLZA8Pf4",
    screenshot_url: "https://4efckk3ja2.execute-api.us-east-1.amazonaws.com/dev/screenshot",
    custom_sewing_variant_id: 40424965079229
  }
};
exports.environments = environments;

var getEnvironmentConfig = function getEnvironmentConfig() {
  var host = location.hostname;
  if (host.endsWith("stoneycloverlane.com")) return environments["PROD"];
  if (host.startsWith("stoney-clover-staging")) return environments["STAGING"];
  if (host.startsWith("stoney-clover-dev")) return environments["DEVELOPMENT"];
  if (host.startsWith("scl-dev")) return environments["SCL_DEV"];
  if (host.startsWith("stoneycloverlane-development")) return environments["TOMORROW_DEVELOPMENT"];
  if (host.startsWith("127.0.0.1")) return environments["TOMORROW_DEVELOPMENT"];
  return environments["PROD"];
};
/*
CRUD operations for AWS Endpoint
----------------------------------------------------------
*/


exports.getEnvironmentConfig = getEnvironmentConfig;

var submitRequest = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee(endpoint, method) {
    var data,
        _getEnvironmentConfig,
        key,
        url,
        fullUrl,
        options,
        response,
        _args = arguments;

    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            data = _args.length > 2 && _args[2] !== undefined ? _args[2] : {};
            _getEnvironmentConfig = getEnvironmentConfig(), key = _getEnvironmentConfig.key, url = _getEnvironmentConfig.url;
            fullUrl = "".concat(url, "/").concat(endpoint);
            options = {
              method: method,
              headers: {
                "Content-Type": "application/json",
                "x-api-key": key
              },
              body: JSON.stringify(data)
            };
            _context.next = 6;
            return fetch(fullUrl, options);

          case 6:
            response = _context.sent;
            return _context.abrupt("return", response);

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function submitRequest(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

var saveItem = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee2(data) {
    var response;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            console.log("saving item for later");
            _context2.next = 3;
            return submitRequest("save-for-later", "POST", data);

          case 3:
            response = _context2.sent;
            return _context2.abrupt("return", response);

          case 5:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function saveItem(_x3) {
    return _ref2.apply(this, arguments);
  };
}();

exports.saveItem = saveItem;

var deleteItem = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee3() {
    var response;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return submitRequest("save-for-later", "DELETE");

          case 2:
            response = _context3.sent;
            return _context3.abrupt("return", response);

          case 4:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function deleteItem() {
    return _ref3.apply(this, arguments);
  };
}();

exports.deleteItem = deleteItem;

var getItemLink = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee4() {
    var response;
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return submitRequest("customizer-link", "POST");

          case 2:
            response = _context4.sent;
            return _context4.abrupt("return", response);

          case 4:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function getItemLink() {
    return _ref4.apply(this, arguments);
  };
}();

exports.getItemLink = getItemLink;

var getItem = /*#__PURE__*/function () {
  var _ref5 = _asyncToGenerator( /*#__PURE__*/_regenerator["default"].mark(function _callee5(id) {
    var data, response;
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            data = {};
            _context5.next = 3;
            return submitRequest("customizer-link?link_id=".concat(id), "GET", data);

          case 3:
            response = _context5.sent;
            return _context5.abrupt("return", response);

          case 5:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function getItem(_x4) {
    return _ref5.apply(this, arguments);
  };
}();

exports.getItem = getItem;

},{"@babel/runtime/helpers/interopRequireDefault":1,"@babel/runtime/regenerator":3}]},{},[4]);
