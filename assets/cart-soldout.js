class CartSoldOut {
  constructor(title, message, buttonText) {
    this.data = {
      title: title || "A note about your cart",
      message: message || "This product is currently out of stock.",
      buttonText: buttonText || "Continue",
    };
    this.soldOutSelector = "[data-soldout]";
    this.soldOutPopupSelector = "#SoldOutModal";
    this.cartIntegritySelector = "[data-cart-integrity]";
  }

  checkSoldOut() {
    const soldOut = document.querySelectorAll(this.soldOutSelector);
    if (soldOut.length) {
      this.renderSoldOut();
      return true;
    }
    return false;
  }

  renderSoldOut() {
    const popupTemplate = this.template();
    document.body.insertAdjacentHTML('beforeend', popupTemplate);
    this.attachEvents();
  }

  attachEvents() {
    const popup = document.querySelector(this.soldOutPopupSelector);
    const buttons = popup.querySelectorAll('button');
    buttons.forEach((button) => {
      button.addEventListener('click', (event) => popup.remove());
    });
  }

  checkProductsWithoutParent() {
    const _self = this;
    const cartIntegrity = document.querySelector(this.cartIntegritySelector);
    if (cartIntegrity) {
      const items = cartIntegrity.dataset.cartIntegrity.split(',');
      const objectItems = items.reduce((acc, item) => {
        const [key, value] = item.split('||');
        acc[key] = parseInt(value);
        return acc;
      }, {});
      _self.removeProducts(objectItems);
    } else {
      console.log('Cart integrity');
    }
  }

  template() {
    return `<div class="sitewide-quantity-limit-modal active" id="SoldOutModal">
        <button type="button" class="close">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
            </svg>
        </button>
        <h3>${this.data.title}</h3>
        <p>${this.data.message}</p>
        <div class="actions">
            <button type="button" class="cancel">${this.data.buttonText}</button>
        </div>
    </div>`;
  }

  removeProducts(productsToRemove) {
    fetch("/cart/update.js", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest",
      },
      body: JSON.stringify({ updates: productsToRemove }),
    })
    .then((data) => {
      window.location.reload();
    })
    .catch((error) => {
      console.error("Error:", error);
    });
  }
}