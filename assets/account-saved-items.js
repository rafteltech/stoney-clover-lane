/*
utilities for saving and loading pre-customized bags via the AWS endpoint
*/

//environments
const environments = {
  PROD: {
    url: "https://7q6dv7yx04.execute-api.us-east-1.amazonaws.com/dev",
    key: "kfzRoNK75R9pM9Rfcr7p3vyiEearfaR7BLZA8Pf4",
    screenshot_url: "https://6u0t12lj6l.execute-api.us-east-1.amazonaws.com/prod/screenshot",
    custom_sewing_variant_id: 4931250225182,
  },
  STAGING: {
    url: "https://neqva6afs4.execute-api.us-west-2.amazonaws.com/dev",
    key: "nDonJkK2hn6DyEGec1MOC2z1jhEX4Wi45ncJJRkT",
    screenshot_url: "https://4efckk3ja2.execute-api.us-east-1.amazonaws.com/dev/screenshot",
    custom_sewing_variant_id: 31344967188589,
  },
  DEVELOPMENT: {
    url: "https://7q6dv7yx04.execute-api.us-east-1.amazonaws.com/dev",
    key: "kfzRoNK75R9pM9Rfcr7p3vyiEearfaR7BLZA8Pf4",
    screenshot_url: "https://4efckk3ja2.execute-api.us-east-1.amazonaws.com/dev/screenshot",
    custom_sewing_variant_id: 31069085597801,
  },
  SCL_DEV: {
    url: "https://7q6dv7yx04.execute-api.us-east-1.amazonaws.com/dev",
    key: "kfzRoNK75R9pM9Rfcr7p3vyiEearfaR7BLZA8Pf4",
    screenshot_url: "https://4efckk3ja2.execute-api.us-east-1.amazonaws.com/dev/screenshot",
    custom_sewing_variant_id: 31069085597801,
  },
  TOMORROW_DEVELOPMENT: {
    url: "https://efpf7kla9c.execute-api.us-east-2.amazonaws.com/beta",
    key: "kfzRoNK75R9pM9Rfcr7p3vyiEearfaR7BLZA8Pf4",
    screenshot_url: "https://4efckk3ja2.execute-api.us-east-1.amazonaws.com/dev/screenshot",
    custom_sewing_variant_id: 40424965079229,
  },
  DEFAULT: {
    url: "https://efpf7kla9c.execute-api.us-east-2.amazonaws.com/beta",
    key: "kfzRoNK75R9pM9Rfcr7p3vyiEearfaR7BLZA8Pf4",
    screenshot_url: "https://4efckk3ja2.execute-api.us-east-1.amazonaws.com/dev/screenshot",
    custom_sewing_variant_id: 4931250225182,
  },
};

const getEnvironmentConfig = () => {
  const host = location.hostname;
  if (host.endsWith("stoneycloverlane.com")) return environments["PROD"];
  if (host.startsWith("stoney-clover-staging")) return environments["STAGING"];
  if (host.startsWith("stoney-clover-dev")) return environments["DEVELOPMENT"];
  if (host.startsWith("scl-dev")) return environments["SCL_DEV"];
  if (host.startsWith("stoneycloverlane-development")) return environments["TOMORROW_DEVELOPMENT"];
  if (host.startsWith("127.0.0.1")) return environments["TOMORROW_DEVELOPMENT"];
  return environments["DEFAULT"];
};

/*
CRUD operations for AWS Endpoint
----------------------------------------------------------
*/
const submitRequest = async (endpoint, method, data) => {
  const { key, url } = getEnvironmentConfig();
  const fullUrl = `${url}/${endpoint}`;
  const options = {
    method,
    headers: {
      "Content-Type": "application/json",
      "x-api-key": key,
    },
  };
  if (data) { 
    options['body'] = JSON.stringify(data)
  }

  const response = await fetch(fullUrl, options);
  return response;
};

const deleteItem = async (id, customer_id) => {
  const response = await submitRequest("save-for-later", "DELETE", {
    id: id,
    customer_id: customer_id,
  });
  return response;
};

const getItem = async (id) => {
  const response = await submitRequest(`customizer-link?link_id=${id}`, "GET");
  return response;
};

const savedItems = document.querySelectorAll("#saved .product")
const customerId = document.querySelector(".saved-items")?.getAttribute('data-customer-id')

for (let i = 0; i < savedItems.length; i++) {
  const item = savedItems[i];

  const deleteButton = item.querySelector(".remove")
  const returnButton = item.querySelector(".return")
  const save_id = returnButton.getAttribute('data-saved-id')
  const link_id = returnButton.getAttribute('data-link-id')

  returnButton.addEventListener("click", (e) => {
    e.preventDefault()
    getItem(link_id)
      .then(response => response.json())
      .then(data => { 
        //saved designs from before redesign need to be redirected
        //old format: https://www.stoneycloverlane.com/collections/customize/products/nylon-small-pouch-2019?variant=21389229228112
        //new format: https://www.stoneycloverlane.com/products/nylon-small-pouch-2019?view=customizer&variant=21389229228112
        const link = data.customizer_link;
        if (!link) return;
        console.log(link);
        if (data.customizer_link.includes("collections")) {
          const newLink = link.replace("collections/customize/", "").replace("?", "?view=customizer&")
          location.href=newLink
        }
        else { 
          location.href=link
        }
      })
  });

  deleteButton.addEventListener("click", (e) => {
    e.preventDefault()
    deleteItem(save_id, customerId).then(res => { 
      console.log(res)
      location.href = `${location.href}?customizations=true`
    })
  });
  
}


