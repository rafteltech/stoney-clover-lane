function getFocusableElements(container) {
  return Array.from(
    container.querySelectorAll(
      "summary, a[href], button:enabled, [tabindex]:not([tabindex^='-']), [draggable], area, input:not([type=hidden]):enabled, select:enabled, textarea:enabled, object, iframe"
    )
  );
}

const trapFocusHandlers = {};

function trapFocus(container, elementToFocus = container) {
  var elements = getFocusableElements(container);
  var first = elements[0];
  var last = elements[elements.length - 1];

  removeTrapFocus();

  trapFocusHandlers.focusin = (event) => {
    if (event.target !== container && event.target !== last && event.target !== first) return;

    document.addEventListener("keydown", trapFocusHandlers.keydown);
  };

  trapFocusHandlers.focusout = function () {
    document.removeEventListener("keydown", trapFocusHandlers.keydown);
  };

  trapFocusHandlers.keydown = function (event) {
    if (event.code.toUpperCase() !== "TAB") return; // If not TAB key
    // On the last focusable element and tab forward, focus the first element.
    if (event.target === last && !event.shiftKey) {
      event.preventDefault();
      first.focus();
    }

    //  On the first focusable element and tab backward, focus the last element.
    if ((event.target === container || event.target === first) && event.shiftKey) {
      event.preventDefault();
      last.focus();
    }
  };

  document.addEventListener("focusout", trapFocusHandlers.focusout);
  document.addEventListener("focusin", trapFocusHandlers.focusin);

  elementToFocus.focus();
}

function pauseAllMedia() {
  document.querySelectorAll(".js-youtube").forEach((video) => {
    video.contentWindow.postMessage(
      '{"event":"command","func":"' + "pauseVideo" + '","args":""}',
      "*"
    );
  });
  document.querySelectorAll(".js-vimeo").forEach((video) => {
    video.contentWindow.postMessage('{"method":"pause"}', "*");
  });
  document.querySelectorAll("video").forEach((video) => video.pause());
  document.querySelectorAll("product-model").forEach((model) => model.modelViewerUI?.pause());
}


function removeTrapFocus(elementToFocus = null) {
  document.removeEventListener("focusin", trapFocusHandlers.focusin);
  document.removeEventListener("focusout", trapFocusHandlers.focusout);
  document.removeEventListener("keydown", trapFocusHandlers.keydown);

  if (elementToFocus) elementToFocus.focus();
}

class QuantityInput extends HTMLElement {
  constructor() {
    super();
    this.input = this.querySelector("input");
    this.changeEvent = new Event("change", { bubbles: true });

    this.querySelectorAll("button").forEach((button) =>
      button.addEventListener("click", this.onButtonClick.bind(this))
    );

    //pdp page has multiple inputs for sticky header
    this.hasMultipleInputs = this.classList.contains("multiple-inputs")
    this.allInputs = document.querySelectorAll(".quantity__input")
  }

  onButtonClick(event) {
    event.preventDefault();
    const previousValue = this.input.value;

    //clicking on a button for one input updates all the others
    if (this.hasMultipleInputs) {
      for (let i = 0; i < this.allInputs.length; i++) {
        const input = this.allInputs[i];
        event.target.name === "plus" ? input.stepUp() : input.stepDown();
      }
    }
    else { 
      event.target.name === "plus" ? this.input.stepUp() : this.input.stepDown();
    }

    if (previousValue !== this.input.value) this.input.dispatchEvent(this.changeEvent);
  }
}


function debounce(fn, wait) {
  let t;
  return (...args) => {
    clearTimeout(t);
    t = setTimeout(() => fn.apply(this, args), wait);
  };
}

const serializeForm = (form) => {
  const obj = {};
  const formData = new FormData(form);
  for (const key of formData.keys()) {
    obj[key] = formData.get(key);
  }
  return JSON.stringify(obj);
};

function fetchConfig(type = "json") {
  return {
    method: "POST",
    headers: { "Content-Type": "application/json", Accept: `application/${type}` },
  };
}

/*
 * Shopify Common JS
 *
 */
if (typeof window.Shopify == "undefined") {
  window.Shopify = {};
}

Shopify.bind = function (fn, scope) {
  return function () {
    return fn.apply(scope, arguments);
  };
};

Shopify.setSelectorByValue = function (selector, value) {
  for (var i = 0, count = selector.options.length; i < count; i++) {
    var option = selector.options[i];
    if (value == option.value || value == option.innerHTML) {
      selector.selectedIndex = i;
      return i;
    }
  }
};

Shopify.addListener = function (target, eventName, callback) {
  target.addEventListener
    ? target.addEventListener(eventName, callback, false)
    : target.attachEvent("on" + eventName, callback);
};

Shopify.postLink = function (path, options) {
  options = options || {};
  var method = options["method"] || "post";
  var params = options["parameters"] || {};

  var form = document.createElement("form");
  form.setAttribute("method", method);
  form.setAttribute("action", path);

  for (var key in params) {
    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", key);
    hiddenField.setAttribute("value", params[key]);
    form.appendChild(hiddenField);
  }
  document.body.appendChild(form);
  form.submit();
  document.body.removeChild(form);
};

Shopify.CountryProvinceSelector = function (country_domid, province_domid, options) {
  this.countryEl = document.getElementById(country_domid);
  this.provinceEl = document.getElementById(province_domid);
  this.provinceContainer = document.getElementById(options["hideElement"] || province_domid);

  Shopify.addListener(this.countryEl, "change", Shopify.bind(this.countryHandler, this));

  this.initCountry();
  this.initProvince();
};

Shopify.CountryProvinceSelector.prototype = {
  initCountry: function () {
    var value = this.countryEl.getAttribute("data-default");
    Shopify.setSelectorByValue(this.countryEl, value);
    this.countryHandler();
  },

  initProvince: function () {
    var value = this.provinceEl.getAttribute("data-default");
    if (value && this.provinceEl.options.length > 0) {
      Shopify.setSelectorByValue(this.provinceEl, value);
    }
  },

  countryHandler: function (e) {
    var opt = this.countryEl.options[this.countryEl.selectedIndex];
    var raw = opt.getAttribute("data-provinces");
    var provinces = JSON.parse(raw);

    this.clearOptions(this.provinceEl);
    if (provinces && provinces.length == 0) {
      this.provinceContainer.style.display = "none";
    } else {
      for (var i = 0; i < provinces.length; i++) {
        var opt = document.createElement("option");
        opt.value = provinces[i][0];
        opt.innerHTML = provinces[i][1];
        this.provinceEl.appendChild(opt);
      }

      this.provinceContainer.style.display = "";
    }
  },

  clearOptions: function (selector) {
    while (selector.firstChild) {
      selector.removeChild(selector.firstChild);
    }
  },

  setOptions: function (selector, values) {
    for (var i = 0, count = values.length; i < values.length; i++) {
      var opt = document.createElement("option");
      opt.value = values[i];
      opt.innerHTML = values[i];
      selector.appendChild(opt);
    }
  },
};

class MenuDrawer extends HTMLElement {
  constructor() {
    super();

    this.mainDetailsToggle = this.querySelector("details");
    const summaryElements = this.querySelectorAll("summary");
    this.addAccessibilityAttributes(summaryElements);

    if (navigator.platform === "iPhone")
      document.documentElement.style.setProperty("--viewport-height", `${window.innerHeight}px`);

    this.addEventListener("keyup", this.onKeyUp.bind(this));
    this.addEventListener("focusout", this.onFocusOut.bind(this));
    this.bindEvents();
  }

  bindEvents() {
    window.addEventListener('resize', (event)=>{
      if (window.innerWidth > 1290) {
        this.closeMenuDrawer(event, false, true)
      }
    })
    
    this.querySelectorAll("summary").forEach((summary) => { 
      summary.addEventListener("click", this.onSummaryClick.bind(this));
    });

    this.querySelectorAll("button").forEach((button) =>
      button.addEventListener("click", this.onCloseButtonClick.bind(this))
    );
  }

  addAccessibilityAttributes(summaryElements) {
    summaryElements.forEach((element) => {
      element.setAttribute("role", "button");
      element.setAttribute("aria-expanded", false);
      element.setAttribute("aria-controls", element.nextElementSibling.id);
    });
  }

  onKeyUp(event) {
    if (event.code.toUpperCase() !== "ESCAPE") return;

    const openDetailsElement = event.target.closest("details[open]");
    if (!openDetailsElement) return;

    openDetailsElement === this.mainDetailsToggle
      ? this.closeMenuDrawer(this.mainDetailsToggle.querySelector("summary"))
      : this.closeSubmenu(openDetailsElement);
  }

  onSummaryClick(event) {
    // const root = document.getElementsByTagName( 'html' )[0];
    // root.classList.add("menu-open");
    const summaryElement = event.currentTarget;
    const detailsElement = summaryElement.parentNode;
    const isOpen = detailsElement.hasAttribute("open");

    if (detailsElement === this.mainDetailsToggle) {
      if (isOpen) event.preventDefault();
      isOpen ? this.closeMenuDrawer(summaryElement, false, false) : this.openMenuDrawer(summaryElement);
    } else {
      trapFocus(summaryElement.nextElementSibling, detailsElement.querySelector("button"));
      setTimeout(() => {
        detailsElement.classList.add("menu-opening");
      });
    }
  }

  openMenuDrawer(summaryElement) {
    // const root = document.getElementsByTagName( 'html' )[0];
    // root.classList.add("menu-open");
    
    setTimeout(() => {
      this.mainDetailsToggle.classList.add("menu-opening");
    });
    summaryElement.setAttribute("aria-expanded", true);
    trapFocus(this.mainDetailsToggle, summaryElement);
    document.body.classList.add(`overflow-hidden-${this.dataset.breakpoint}`);
  }

  closeMenuDrawer(event, elementToFocus = false, isFullMenu) {
    if (isFullMenu) {
      
      const root = document.getElementsByTagName('html')[0];
      root.classList.remove("menu-open");
      
      this.mainDetailsToggle.classList.remove("menu-opening");
      
      this.mainDetailsToggle.querySelector("summary").setAttribute("aria-expanded", false);
      document.body.classList.remove(`overflow-hidden-${this.dataset.breakpoint}`);
      removeTrapFocus(elementToFocus);
      this.closeAnimation(this.mainDetailsToggle);
      if (document.querySelector(".compare-sizes__tab-list")) {
        document.querySelector(".compare-sizes__tab-list").style.zIndex = "5";
      }
    }
    
    this.mainDetailsToggle.querySelectorAll("details").forEach((details) => {
      details.removeAttribute("open");
      details.classList.remove("menu-opening");
    });
  }

  onFocusOut(event) {
    //console.log(1)
    // setTimeout(() => {
    //   if (
    //     this.mainDetailsToggle.hasAttribute("open") &&
    //     !this.mainDetailsToggle.contains(document.activeElement)
    //   )
    //     this.closeMenuDrawer();
    // });
  }

  onCloseButtonClick(event) {
    const detailsElement = event.currentTarget.closest("details");
    this.closeSubmenu(detailsElement);
    const root = document.getElementsByTagName('html')[0];
    root.classList.remove("menu-open");
  }

  closeSubmenu(detailsElement) {
    removeTrapFocus();
    this.closeAnimation(detailsElement);
    setTimeout(() => {
      detailsElement.classList.remove("menu-opening");
    }, 500);
  }

  closeAnimation(detailsElement) {
    let animationStart;

    const handleAnimation = (time) => {
      if (animationStart === undefined) {
        animationStart = time;
      }

      const elapsedTime = time - animationStart;

      if (elapsedTime < 400) {
        window.requestAnimationFrame(handleAnimation);
      } else {
        detailsElement.removeAttribute("open");
        if (detailsElement.closest("details[open]")) {
          trapFocus(
            detailsElement.closest("details[open]"),
            detailsElement.querySelector("summary")
          );
        }
      }
    };

    window.requestAnimationFrame(handleAnimation);
  }
}



class HeaderDrawer extends MenuDrawer {
  constructor() {
    super();
  }


  openMenuDrawer(summaryElement) {
    const root = document.getElementsByTagName( 'html' )[0];
    root.classList.add("menu-open");
    
    this.header = this.header || document.getElementById("shopify-section-header");
    this.borderOffset =
      this.borderOffset ||
      this.closest(".header-wrapper").classList.contains("header-wrapper--border-bottom")
        ? 1
        : 0;
    document.documentElement.style.setProperty(
      "--header-bottom-position",
      `${parseInt(this.header.getBoundingClientRect().bottom - this.borderOffset)}px`
    );

    setTimeout(() => {
      this.mainDetailsToggle.classList.add("menu-opening");
      if (document.querySelector(".compare-sizes__tab-list")) {
        document.querySelector(".compare-sizes__tab-list").style.zIndex = "1";
      }
    });

    summaryElement.setAttribute("aria-expanded", true);
    trapFocus(this.mainDetailsToggle, summaryElement);
    document.body.classList.add(`overflow-hidden-${this.dataset.breakpoint}`);
  }
}


class ModalDialog extends HTMLElement {
  constructor() {
    super();
    this.querySelector('[id^="ModalClose-"]').addEventListener("click", this.hide.bind(this));
    this.addEventListener("click", (event) => {
      if (event.target.nodeName === "MODAL-DIALOG") this.hide();
    });
    this.addEventListener("keyup", () => {
      if (event.code.toUpperCase() === "ESCAPE") this.hide();
    });
  }

  show(opener) {
    this.openedBy = opener;
    document.body.classList.add("overflow-hidden");
    this.setAttribute("open", "");
    this.querySelector(".template-popup")?.loadContent();
    trapFocus(this, this.querySelector('[role="dialog"]'));
  }

  hide() {
    document.body.classList.remove("overflow-hidden");
    this.removeAttribute("open");
    removeTrapFocus(this.openedBy);
    window.pauseAllMedia();
  }
}


class ModalOpener extends HTMLElement {
  constructor() {
    super();

    const button = this.querySelector("button");
    button?.addEventListener("click", () => {
      document.querySelector(this.getAttribute("data-modal"))?.show(button);
    });
  }
}


class DeferredMedia extends HTMLElement {
  constructor() {
    super();
    this.querySelector('[id^="Deferred-Poster-"]')?.addEventListener(
      "click",
      this.loadContent.bind(this)
    );
  }

  loadContent() {
    if (!this.getAttribute("loaded")) {
      const content = document.createElement("div");
      content.appendChild(this.querySelector("template").content.firstElementChild.cloneNode(true));

      this.setAttribute("loaded", true);
      window.pauseAllMedia();
      this.appendChild(content.querySelector("video, model-viewer, iframe")).focus();
    }
  }
}



class SliderComponent extends HTMLElement {
  constructor() {
    super();
    this.slider = this.querySelector("ul");
    this.sliderItems = this.querySelectorAll("li");
    this.pageCount = this.querySelector(".slider-counter--current");
    this.pageTotal = this.querySelector(".slider-counter--total");
    this.prevButton = this.querySelector('button[name="previous"]');
    this.nextButton = this.querySelector('button[name="next"]');

    if (!this.slider || !this.nextButton) return;

    const resizeObserver = new ResizeObserver((entries) => this.initPages());
    resizeObserver.observe(this.slider);

    this.slider.addEventListener("scroll", this.update.bind(this));
    this.prevButton.addEventListener("click", this.onButtonClick.bind(this));
    this.nextButton.addEventListener("click", this.onButtonClick.bind(this));
  }

  initPages() {
    if (!this.sliderItems.length === 0) return;
    this.slidesPerPage = Math.floor(this.slider.clientWidth / this.sliderItems[0].clientWidth);
    this.totalPages = this.sliderItems.length - this.slidesPerPage + 1;
    this.update();
  }

  update() {
    if (!this.pageCount || !this.pageTotal) return;
    this.currentPage = Math.round(this.slider.scrollLeft / this.sliderItems[0].clientWidth) + 1;

    if (this.currentPage === 1) {
      this.prevButton.setAttribute("disabled", true);
    } else {
      this.prevButton.removeAttribute("disabled");
    }

    if (this.currentPage === this.totalPages) {
      this.nextButton.setAttribute("disabled", true);
    } else {
      this.nextButton.removeAttribute("disabled");
    }

    this.pageCount.textContent = this.currentPage;
    this.pageTotal.textContent = this.totalPages;
  }

  onButtonClick(event) {
    event.preventDefault();
    const slideScrollPosition =
      event.currentTarget.name === "next"
        ? this.slider.scrollLeft + this.sliderItems[0].clientWidth
        : this.slider.scrollLeft - this.sliderItems[0].clientWidth;
    this.slider.scrollTo({
      left: slideScrollPosition,
    });
  }
}



class VariantSelects extends HTMLElement {
  constructor() {
    super();
    this.addEventListener("change", this.onVariantChange);
  }

  onVariantChange() {
    this.updateOptions();
    this.updateMasterId();
    // this.toggleAddButton(true, '', false);
    this.updatePickupAvailability();

    if (!this.currentVariant) {
      // this.toggleAddButton(true, '', true);
      this.setUnavailable();
    } else {
      this.updateMedia();
      this.updateURL();
      this.updateVariantInput();
      this.renderProductInfo();
    }
  }

  updateOptions() {
    this.options = Array.from(this.querySelectorAll("select"), (select) => select.value);
  }

  updateMasterId() {
    this.currentVariant = this.getVariantData().find((variant) => {
      return !variant.options
        .map((option, index) => {
          return this.options[index] === option;
        })
        .includes(false);
    });
  }

  updateMedia() {
    if (!this.currentVariant || !this.currentVariant?.featured_media) return;
    const newMedia = document.querySelector(
      `[data-media-id="${this.dataset.section}-${this.currentVariant.featured_media.id}"]`
    );
    if (!newMedia) return;
    const parent = newMedia.parentElement;
    parent.prepend(newMedia);
    window.setTimeout(() => {
      parent.scroll(0, 0);
    });
  }

  updateURL() {
    if (!this.currentVariant) return;
    window.history.replaceState({}, "", `${this.dataset.url}?variant=${this.currentVariant.id}`);
  }

  updateVariantInput() {
    const productForms = document.querySelectorAll(
      `#product-form-${this.dataset.section}, #product-form-installment`
    );
    productForms.forEach((productForm) => {
      const input = productForm.querySelector('input[name="id"]');
      input.value = this.currentVariant.id;
      input.dispatchEvent(new Event("change", { bubbles: true }));
    });
  }

  updatePickupAvailability() {
    const pickUpAvailability = document.querySelector("pickup-availability");
    if (!pickUpAvailability) return;

    if (this.currentVariant?.available) {
      pickUpAvailability.fetchAvailability(this.currentVariant.id);
    } else {
      pickUpAvailability.removeAttribute("available");
      pickUpAvailability.innerHTML = "";
    }
  }

  renderProductInfo() {
    fetch(
      `${this.dataset.url}?variant=${this.currentVariant.id}&section_id=${this.dataset.section}`
    )
      .then((response) => response.text())
      .then((responseText) => {
        // const id = `price-${this.dataset.section}`;
        // const html = new DOMParser().parseFromString(responseText, 'text/html')
        // const destination = document.getElementById(id);
        // const source = html.getElementById(id);

        // if (source && destination) destination.innerHTML = source.innerHTML;

        document
          .getElementById(`price-${this.dataset.section}`)
          ?.classList.remove("visibility-hidden");
        // this.toggleAddButton(!this.currentVariant.available, window.variantStrings.soldOut);
      });
  }

  toggleAddButton(disable = true, text, modifyClass = true) {
    const addButton = document
      .getElementById(`product-form-${this.dataset.section}`)
      ?.querySelector('[name="add"]');

    if (!addButton) return;

    if (disable) {
      addButton.setAttribute("disabled", true);
      if (text) addButton.textContent = text;
    } else {
      addButton.removeAttribute("disabled");
      addButton.textContent = window.variantStrings.addToCart;
    }

    if (!modifyClass) return;
  }

  setUnavailable() {
    const addButton = document
      .getElementById(`product-form-${this.dataset.section}`)
      ?.querySelector('[name="add"]');
    if (!addButton) return;
    addButton.textContent = window.variantStrings.unavailable;
    document.getElementById(`price-${this.dataset.section}`)?.classList.add("visibility-hidden");
  }

  getVariantData() {
    this.variantData =
      this.variantData || JSON.parse(this.querySelector('[type="application/json"]').textContent);
    return this.variantData;
  }
}


class VariantRadios extends VariantSelects {
  constructor() {
    super();
    this.addEventListener("change", this.onVariantChange);
    this.shopPayNeedsRender = false;
    this.previousVariant;
    this.stickyInfoBar = document.querySelector('.sticky-info-bar');
    this.stickyBarKlaviyoButton = this.stickyInfoBar.querySelector('.notify-button');
  }

  connectedCallback(){
    // Hack-around Klaviyo onsitejs library, that doesn't allow to use multiple OOS buttons at the same page.
    this.stickyBarKlaviyoButton?.addEventListener('click', function (e){
      e.preventDefault()
      let klaviyoButton = document.querySelector('.notify-button.klaviyo-bis-trigger');
      klaviyoButton?.dispatchEvent(new Event('click', { bubbles: true }))
    })
  }

  onVariantChange() {
    this.onOptionSelect();
    this.updateCurrentVariant();
    //this.updateCurrentVariant();
    this.updateUrl();
    this.updateImages();
    this.updateSoldOutBadge();
    this.updateForm();
    this.updatePrice();
    this.updateTitle();
    this.updateShopPayWidget();
  }

  async updateShopPayWidget() {
    const currentShopPayWidgets = document.querySelectorAll('shopify-payment-terms');
    const handle = this.currentVariant.dataset.handle;
    const parser = new DOMParser();

    if (!handle) return;

    if (this.previousVariant && this.previousVariant.dataset.seasonal
      || this.currentVariant.dataset.seasonal) {
      this.shopPayNeedsRender = true;
    } else {
      this.shopPayNeedsRender = false;
    }

    if (!this.shopPayNeedsRender) return;

    currentShopPayWidgets.forEach((widget) => widget.style.opacity = 0);
    const productPage = await (await fetch(`/products/${handle}`)).text();
    const doc = parser.parseFromString(productPage, 'text/html');
    const newShopPayWidget = doc.querySelector('shopify-payment-terms');
    const variantId = newShopPayWidget.getAttribute('variant-id');
    const shopifyMeta = newShopPayWidget.getAttribute('shopify-meta');

    currentShopPayWidgets.forEach((widget) => {
      widget.setAttribute('variant-id', variantId);
      widget.setAttribute('shopify-meta', shopifyMeta);
      widget.style.opacity = 1;
    });
  }

  updateTitle() {
    const productTitle = document.querySelectorAll('.product__title');
    this.productTitle = JSON.parse(
      document.querySelector('.product__media-wrapper [type="application/json"]').innerHTML
    )?.find((variant) => variant.id === this.currentVariant.dataset.id)?.productTitle;
    
    productTitle.forEach((title) => title.innerText = this.productTitle);
  }

  updatePrice() {
    const priceTexts = document.querySelectorAll('.price-item.price-item--regular');
    const priceSalesTexts = document.querySelectorAll('.price-item.price-item--sale');
    const shopPay = document.querySelector('shopify-payment-terms');
    const currentVariant = JSON.parse(
      document.querySelector('.product__media-wrapper [type="application/json"]').innerHTML
    )?.find((variant) => variant.id === this.currentVariant.dataset.id);

    this.variantPrice = currentVariant?.ss_price || currentVariant?.price;
    let variantSalePrice = currentVariant?.ss_compare_at_price || currentVariant?.compare_at_price;

    // Update price texts and update Shop Pay widget
    if (variantSalePrice && variantSalePrice.replace(/[$,USD,\s]+/g,"") !== '0.00') {
      priceTexts.forEach((price) => price.innerText = variantSalePrice);
      priceSalesTexts.forEach((price) => price.innerText = this.variantPrice);
    } else {
      priceTexts.forEach((price) => price.innerText = this.variantPrice);
    }

    shopPay.setAttribute('variant-id', this.currentVariant.dataset.id);
  }

  updateCurrentVariant() {
    this.previousVariant = this.currentVariant;
    const inputs = document.querySelectorAll('[data-type="add-to-cart-form"] [name="id"]');
    this.currentVariant = this.querySelector("input:checked");
    inputs.forEach((input) => (input.value = this.currentVariant.dataset.id));
  }

  updateOptions() {
    const fieldsets = Array.from(this.querySelectorAll("fieldset"));
    this.options = fieldsets.map((fieldset) => {
      return Array.from(fieldset.querySelectorAll("input")).find((radio) => radio.checked).value;
    });
  }

  updateUrl() {
    if (this.currentVariant.dataset.seasonal) return;
    window.history.replaceState(
      {},
      "",
      `${this.dataset.url}?variant=${this.currentVariant.dataset.id}`
    );
  }

  onOptionSelect() {
    this.selectedOption = this.querySelector("input:checked");
    const value = this.selectedOption.value
    const wrapper = document.querySelector('legend.form__label.optionName .selectedValue');
    wrapper.innerHTML = value;
  }

  updateImages() {
    this.variantImages = JSON.parse(
      document.querySelector('.product__media-wrapper [type="application/json"]').innerHTML
    )?.find((variant) => variant.id === this.currentVariant.dataset.id)?.media;

    if (this.variantImages.length) {
      this.currentImagesNodes = document.querySelectorAll("[data-metafield-media]");
      this.currentImagesNodes.forEach((img) => img.remove());
      this.slider = document.querySelector("slider-component ul");

      this.variantImages.forEach((img, index) =>
        this.slider.insertAdjacentHTML("afterbegin", this.renderImage(img, index))
      );
      setTimeout(() => {
        document.querySelectorAll("[data-metafield-media]").forEach((img) =>
          img.classList.remove("loading")
        );
      }, 100)

    }
    //letters and numbers
    else { 
      const allVariants = this.getVariantData();
      const currentId = parseInt(this.currentVariant.dataset.id)
      const variantData = allVariants.find(variant => currentId===variant.id)
      
      if (!variantData || !variantData?.featured_media) return;
      const thumbnail = document.querySelector(
        `[data-media-id="${variantData.featured_media.id}"]`
      );
      if (!thumbnail) return;
      const listItem = thumbnail.parentElement.parentElement;
      const letterFeaturedImage = this.currentVariant.dataset.image
      const mainImage = document.querySelector(".main-image-wrap img");

      if (window.innerWidth < 750) {
        listItem.parentNode.scrollLeft = listItem.offsetLeft
      }
      else { 
        mainImage.removeAttribute("srcset")
        mainImage.src = letterFeaturedImage;
      }
    }
  }

  updateSoldOutBadge() {
    this.available = this.selectedOption.dataset.available
    const productBadges = document.querySelectorAll('.product__badge-wrapper')
    if (this.available === 'false') {
      productBadges.forEach(badge => {
        badge.innerHTML = this.renderSoldOutBadge()
      })
    } else {
      productBadges.forEach(badge => {
        badge.innerHTML = this.renderBadge()
      })
    }
  }

  updateForm() {
    this.productInfoContainer = document.querySelector('.product__info-container');
    this.submitButtons = document.querySelectorAll('.product-form__submit');
    this.qtySelectors = document.querySelectorAll('.product-form__quantity');
    this.klaviyoButton = document.querySelector('.notify-button.klaviyo-bis-trigger');
    if (this.klaviyoButton) {
      this.klaviyoButton.style.opacity = '0';
      this.klaviyoButton.style.height = '0';
    }
    if (this.available === 'false') {
      this.productInfoContainer?.classList.add('product__info-container--sold-out');
      this.stickyInfoBar?.classList.add('sticky-info-bar--sold-out');
      this.submitButtons.forEach(btn => {
        btn.classList.add('sold-out')
        btn.disabled = true
        const btnText = btn.querySelector('span')
        btnText.innerHTML = 'Sold out'
      })
      this.qtySelectors.forEach(qty => qty.classList.add('sold-out'))
      if (this.klaviyoButton) {
        this.klaviyoButton.style.opacity = '1';
        this.klaviyoButton.style.removeProperty('height');
      }
    } else {
      this.productInfoContainer?.classList.remove('product__info-container--sold-out');
      this.stickyInfoBar?.classList.remove('sticky-info-bar--sold-out');
      this.submitButtons.forEach(btn => {
        btn.classList.remove('sold-out');
        btn.disabled = false
        const btnText = btn.querySelector('span')
        btnText.innerHTML = 'Add to bag'
      })
      this.qtySelectors.forEach(qty => qty.classList.remove('sold-out'))
    }
  }

  renderSoldOutBadge() {
    return `<svg class="product__badge" width="120" height="120" viewBox="0 0 120 120" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M41.7669 9.88515C50.567 -2.88755 69.433 -2.88754 78.2331 9.88515C93.1403 5.82253 107.539 17.8413 106.207 33.2346L106.168 33.6867C120.261 40.3528 123.444 59.0493 112.5 70.1528L112.667 70.516C119.143 84.6033 109.711 100.855 94.2669 102.221C90.3301 117.164 72.5908 123.845 60 114.885C47.4092 123.845 29.6699 117.164 25.7331 102.221C10.2887 100.855 0.856934 84.6033 7.33305 70.516L7.5 70.1528C-3.44373 59.0493 -0.260886 40.3527 13.8323 33.6867L13.7932 33.2347C12.4606 17.8413 26.8597 5.82253 41.7669 9.88515Z" fill="#CBCBCB"/>
    <path d="M45.5204 18.5706C52.6794 8.11277 68.1101 8.11277 75.2691 18.5706C87.4242 15.2366 99.1831 25.0299 98.1031 37.5876L98.0579 38.113C109.584 43.6 112.155 58.9329 103.224 68.0535L103.417 68.4763C108.679 79.9976 100.966 93.2601 88.3493 94.3829L88.2624 94.7151C85.0699 106.911 70.6443 112.122 60.3947 104.781C50.1452 112.122 35.7196 106.911 32.5271 94.7151L32.4401 94.3829C19.8238 93.2601 12.1102 79.9976 17.3727 68.4763L17.5658 68.0535C8.63401 58.9329 11.2053 43.6 22.7316 38.113L22.6864 37.5876C21.6063 25.0299 33.3652 15.2366 45.5204 18.5706Z" fill="#F1F1F1"/>
    <path d="M37.8559 59.3939C36.9435 59.3939 36.1186 59.2378 35.3811 58.9256C34.6561 58.6134 34.0812 58.1638 33.6562 57.5769C33.2312 56.9899 33.0125 56.2968 33 55.4975H35.8123C35.8498 56.0345 36.0373 56.4591 36.3748 56.7713C36.7248 57.0836 37.1997 57.2397 37.7997 57.2397C38.4122 57.2397 38.8934 57.0961 39.2434 56.8088C39.5933 56.5091 39.7683 56.1219 39.7683 55.6474C39.7683 55.2602 39.6496 54.9418 39.4121 54.692C39.1746 54.4422 38.8746 54.2487 38.5122 54.1113C38.1622 53.9614 37.6747 53.7991 37.0497 53.6242C36.1998 53.3745 35.5061 53.1309 34.9686 52.8937C34.4437 52.6439 33.9874 52.2755 33.6 51.7884C33.225 51.2889 33.0375 50.627 33.0375 49.8028C33.0375 49.0285 33.2312 48.3541 33.6187 47.7796C34.0062 47.2051 34.5499 46.768 35.2499 46.4683C35.9498 46.1561 36.7498 46 37.6497 46C38.9996 46 40.0933 46.3309 40.9308 46.9928C41.7807 47.6422 42.2494 48.5539 42.3369 49.7278H39.4496C39.4246 49.2782 39.2309 48.9098 38.8684 48.6226C38.5184 48.3229 38.0497 48.173 37.4622 48.173C36.9498 48.173 36.5373 48.3041 36.2248 48.5664C35.9248 48.8286 35.7748 49.2095 35.7748 49.7091C35.7748 50.0588 35.8873 50.3522 36.1123 50.5895C36.3498 50.8143 36.6373 51.0017 36.9748 51.1515C37.3247 51.2889 37.8122 51.4512 38.4372 51.6386C39.2871 51.8883 39.9808 52.1381 40.5183 52.3879C41.0557 52.6376 41.5182 53.0123 41.9057 53.5118C42.2932 54.0114 42.4869 54.667 42.4869 55.4788C42.4869 56.1781 42.3057 56.8275 41.9432 57.427C41.5807 58.0264 41.0495 58.5073 40.3495 58.8694C39.6496 59.2191 38.8184 59.3939 37.8559 59.3939Z" fill="#2C272D"/>
    <path d="M53.6495 59.3939C52.4246 59.3939 51.2996 59.1067 50.2747 58.5322C49.2498 57.9578 48.4373 57.1647 47.8374 56.1532C47.2374 55.1291 46.9374 53.9739 46.9374 52.6876C46.9374 51.4138 47.2374 50.2711 47.8374 49.2595C48.4373 48.2354 49.2498 47.4362 50.2747 46.8617C51.2996 46.2872 52.4246 46 53.6495 46C54.8869 46 56.0118 46.2872 57.0243 46.8617C58.0492 47.4362 58.8554 48.2354 59.4429 49.2595C60.0429 50.2711 60.3428 51.4138 60.3428 52.6876C60.3428 53.9739 60.0429 55.1291 59.4429 56.1532C58.8554 57.1647 58.0492 57.9578 57.0243 58.5322C55.9994 59.1067 54.8744 59.3939 53.6495 59.3939ZM53.6495 57.0523C54.4369 57.0523 55.1307 56.8775 55.7306 56.5278C56.3306 56.1657 56.7993 55.6536 57.1368 54.9917C57.4743 54.3298 57.643 53.5618 57.643 52.6876C57.643 51.8134 57.4743 51.0516 57.1368 50.4022C56.7993 49.7403 56.3306 49.2345 55.7306 48.8848C55.1307 48.5352 54.4369 48.3603 53.6495 48.3603C52.862 48.3603 52.1621 48.5352 51.5496 48.8848C50.9497 49.2345 50.4809 49.7403 50.1435 50.4022C49.806 51.0516 49.6372 51.8134 49.6372 52.6876C49.6372 53.5618 49.806 54.3298 50.1435 54.9917C50.4809 55.6536 50.9497 56.1657 51.5496 56.5278C52.1621 56.8775 52.862 57.0523 53.6495 57.0523Z" fill="#2C272D"/>
    <path d="M67.7328 57.1835H72.045V59.2628H65.108V46.1873H67.7328V57.1835Z" fill="#2C272D"/>
    <path d="M81.1004 46.1873C82.4753 46.1873 83.6815 46.4558 84.7189 46.9928C85.7689 47.5298 86.5751 48.2979 87.1376 49.297C87.7125 50.2836 88 51.4325 88 52.7438C88 54.0551 87.7125 55.204 87.1376 56.1906C86.5751 57.1647 85.7689 57.9203 84.7189 58.4573C83.6815 58.9943 82.4753 59.2628 81.1004 59.2628H76.5257V46.1873H81.1004ZM81.0067 57.0336C82.3816 57.0336 83.444 56.659 84.194 55.9096C84.9439 55.1603 85.3189 54.1051 85.3189 52.7438C85.3189 51.3826 84.9439 50.321 84.194 49.5592C83.444 48.7849 82.3816 48.3978 81.0067 48.3978H79.1505V57.0336H81.0067Z" fill="#2C272D"/>
    <path d="M45.6062 80C44.3813 80 43.2563 79.7128 42.2314 79.1383C41.2064 78.5638 40.394 77.7708 39.794 76.7592C39.1941 75.7352 38.8941 74.58 38.8941 73.2937C38.8941 72.0198 39.1941 70.8771 39.794 69.8656C40.394 68.8415 41.2064 68.0422 42.2314 67.4678C43.2563 66.8933 44.3813 66.6061 45.6062 66.6061C46.8436 66.6061 47.9685 66.8933 48.981 67.4678C50.0059 68.0422 50.8121 68.8415 51.3996 69.8656C51.9995 70.8771 52.2995 72.0198 52.2995 73.2937C52.2995 74.58 51.9995 75.7352 51.3996 76.7592C50.8121 77.7708 50.0059 78.5638 48.981 79.1383C47.956 79.7128 46.8311 80 45.6062 80ZM45.6062 77.6584C46.3936 77.6584 47.0873 77.4836 47.6873 77.1339C48.2873 76.7717 48.756 76.2597 49.0935 75.5978C49.4309 74.9359 49.5997 74.1679 49.5997 73.2937C49.5997 72.4195 49.4309 71.6577 49.0935 71.0083C48.756 70.3464 48.2873 69.8406 47.6873 69.4909C47.0873 69.1412 46.3936 68.9664 45.6062 68.9664C44.8187 68.9664 44.1188 69.1412 43.5063 69.4909C42.9063 69.8406 42.4376 70.3464 42.1001 71.0083C41.7627 71.6577 41.5939 72.4195 41.5939 73.2937C41.5939 74.1679 41.7627 74.9359 42.1001 75.5978C42.4376 76.2597 42.9063 76.7717 43.5063 77.1339C44.1188 77.4836 44.8187 77.6584 45.6062 77.6584Z" fill="#2C272D"/>
    <path d="M59.652 66.7934V74.8859C59.652 75.7726 59.8832 76.4533 60.3457 76.9278C60.8082 77.3899 61.4581 77.6209 62.2956 77.6209C63.1455 77.6209 63.8017 77.3899 64.2642 76.9278C64.7267 76.4533 64.9579 75.7726 64.9579 74.8859V66.7934H67.6015V74.8672C67.6015 75.9787 67.3578 76.9216 66.8703 77.6959C66.3953 78.4577 65.7516 79.0321 64.9391 79.4193C64.1392 79.8064 63.2455 80 62.2581 80C61.2831 80 60.3957 79.8064 59.5957 79.4193C58.8083 79.0321 58.1833 78.4577 57.7209 77.6959C57.2584 76.9216 57.0271 75.9787 57.0271 74.8672V66.7934H59.652Z" fill="#2C272D"/>
    <path d="M81.8556 66.7934V68.9102H78.3683V79.8689H75.7435V68.9102H72.2562V66.7934H81.8556Z" fill="#2C272D"/>
    </svg>`
  }

  renderBadge() {
    this.badge = this.selectedOption.dataset.badge;
    return this.badge ? `<img class="product__badge" src="${this.badge}">` : ''
  }

  renderImage(src, index) {
    return `<li data-metafield-media class="loading product__media-item grid__item slider__slide  " data-media-id="${this.dataset.section}-${this.currentVariant.dataset.id}-${index}">
    <modal-opener class="product__modal-opener product__modal-opener--image  no-js-hidden">
      <span class="product__media-icon motion-reduce" aria-hidden="true"><svg width="19" height="19" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg" class="icon icon-plus">
      <circle cx="20" cy="20" r="20" fill="white"></circle>
      <line x1="21" y1="13" x2="21" y2="28" stroke="#9D9D9D" stroke-width="2" stroke-linecap="round"></line>
      <line x1="28" y1="21" x2="13" y2="21" stroke="#9D9D9D" stroke-width="2" stroke-linecap="round"></line>
    </svg></span>
    
      <div class="product__media media media--transparent" style="padding-top: 100%;">
        <img src="${src}" sizes="(min-width: 1600px) 960px, (min-width: 750px) calc((100vw - 11.5rem) / 2), calc(100vw - 4rem)" loading="lazy" width="576" alt="">
      </div>
      <button class="product__media-toggle" type="button" aria-haspopup="dialog" data-media-id="">
        <span class="visually-hidden">Open media 2 in gallery view
    </span>
      </button>
    </modal-opener>
                </li>`;
  }
}

document.addEventListener("DOMContentLoaded", function () {
  console.log("loading shopify custom elements")
  customElements.define("quantity-input", QuantityInput);
  customElements.define("menu-drawer", MenuDrawer);
  customElements.define("header-drawer", HeaderDrawer);
  customElements.define("modal-dialog", ModalDialog);
  customElements.define("variant-radios", VariantRadios);
  customElements.define("deferred-media", DeferredMedia);
  customElements.define("modal-opener", ModalOpener);
  customElements.define("slider-component", SliderComponent);
  customElements.define("variant-selects", VariantSelects);  
});
